<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\MainCategory;

Route::get('/test', function () {
    return view('welcome');
});

Route::get('/check', 'Front\PageController@check');


Route::get('/', 'Front\PageController@showIndex')->name('user.dashboard');
Route::get('/job', 'Front\PageController@showJob')->name('job');
Route::get('/about', 'Front\PageController@showAbout')->name('about');
Route::get('/becomaprofessional', 'Front\PageController@showBecomaprofessional')->name('becomaprofessional');
Route::get('/blog', 'Front\PageController@showBlog')->name('blog');
Route::get('/blog-single', 'Front\PageController@showBlogSingle')->name('blog-single');
Route::get('/professionals', 'Front\PageController@showProfessionals')->name('professionals');
Route::get('/contact-candidate', 'Front\PageController@showContactCandidate')->name('contact-candidate');
Route::get('/support', 'Front\PageController@showSupport')->name('support');
Route::get('/category', 'Front\PageController@showCategory')->name('category');
Route::get('/contact', 'Front\PageController@showContact')->name('contact');
Route::get('/profile', 'Front\PageController@showProfile')->name('profile');
Route::get('/team-member', 'Front\PageController@showMembers')->name('team-member');
Route::get('/protocol', 'Front\PageController@showProtocol')->name('protocol');

//Support
Route::post('show-searchedItem', 'Back\SupportController@showSearchedItem')->name('searchedItem.show');

//Message
Route::get('create-message', 'Front\MessageController@create')->name('message.create');
Route::post('store-message', 'Front\MessageController@store')->name('message.store');
Route::get('destroy-message/{message}', 'Front\MessageController@destroy')->name('message.destroy');

//Filter1
Route::get('employersCat', 'Back\EmployerController@employersCat');

//Search Functionality
Route::post('employer/search', 'Front\PageController@showSearchedEmployer');


//Back
Route::get('/admin', 'Back\PageController@showAdminIndex')->name('admin.dashboard');


// MainCategory
Route::get('/admin/create-maincategory', 'Back\MainCategoryController@create')->name('maincategory.create');
Route::post('/admin/store-maincategory', 'Back\MainCategoryController@store')->name('maincategory.store');
Route::get('/admin/edit-maincategory/{maincategory}', 'Back\MainCategoryController@edit')->name('maincategory.edit');
Route::patch('/admin/update-maincategory/{maincategory}', 'Back\MainCategoryController@update')->name('maincategory.update');
Route::get('/admin/destroy-maincategory/{maincategory}', 'Back\MainCategoryController@destroy')->name('maincategory.destroy');

// SubCategory
Route::get('/admin/create-subcategory', 'Back\SubCategoryController@create')->name('subcategory.create');
Route::post('/admin/store-subcategory', 'Back\SubCategoryController@store')->name('subcategory.store');
Route::get('/admin/edit-subcategory/{subcategory}', 'Back\SubCategoryController@edit')->name('subcategory.edit');
Route::patch('/admin/update-subcategory/{subcategory}', 'Back\SubCategoryController@update')->name('subcategory.update');
Route::get('/admin/destroy-subcategory/{subcategory}', 'Back\SubCategoryController@destroy')->name('subcategory.destroy');

//Job
Route::get('/admin/create-job', 'Back\JobController@create')->name('job.create');
Route::post('/admin/store-job', 'Back\JobController@store')->name('job.store');
Route::get('/admin/getSubCategory/{maincategory_id}', 'Back\MainCategoryController@getSubCategory');
Route::get('/admin/edit-job/{job}', 'Back\JobController@edit')->name('job.edit');
Route::patch('/admin/update-job/{job}', 'Back\JobController@update')->name('job.update');
Route::get('/admin/destroy-job/{job}', 'Back\JobController@destroy')->name('job.destroy');
Route::get('/admin/subcategory/{id}', 'Back\JobController@showJob');

//Support
Route::get('admin/create-support', 'Back\SupportController@create')->name('support.create');
Route::post('/admin/store-support', 'Back\SupportController@store')->name('support.store');
Route::get('/admin/edit-support/{support}', 'Back\SupportController@edit')->name('support.edit');
Route::patch('/admin/update-support/{support}', 'Back\SupportController@update')->name('support.update');
Route::get('admin/destroy-support/{support}', 'Back\SupportController@destroy')->name('support.destroy');

// MainCatEmployer
Route::get('/admin/create-maincatemployer', 'Back\MainCatEmployerController@create')->name('maincatemployer.create');
Route::post('/admin/store-maincatemployer', 'Back\MainCatEmployerController@store')->name('maincatemployer.store');
Route::get('/admin/edit-maincatemployer/{maincatemployer}', 'Back\MainCatEmployerController@edit')->name('maincatemployer.edit');
Route::patch('/admin/update-maincatemployer/{maincatemployer}', 'Back\MainCatEmployerController@update')->name('maincatemployer.update');
Route::get('/admin/destroy-maincatemployer/{maincatemployer}', 'Back\MainCatEmployerController@destroy')->name('maincatemployer.destroy');

// Employer
Route::get('/admin/create-employer', 'Back\EmployerController@create')->name('employer.create');
Route::post('/admin/store-catemployer', 'Back\EmployerController@store')->name('employer.store');

//Team Member
Route::get('/admin/create-teamMember', 'Back\TeamMemberController@create')->name('teamMember.create');
Route::post('/admin/store-teamMember', 'Back\TeamMemberController@store')->name('teamMember.store');
Route::get('/admin/edit-teamMember/{teamMember}', 'Back\TeamMemberController@edit')->name('teamMember.edit');
Route::patch('/admin/update-teamMember/{teamMember}', 'Back\TeamMemberController@update')->name('teamMember.update');
Route::get('/admin/destroy-teamMember/{teamMember}', 'Back\TeamMemberController@destroy')->name('teamMember.destroy');

//Testimonial
Route::get('/admin/create-testimonial', 'Back\TestimonialController@create')->name('testimonial.create');
Route::post('/admin/store-testimonial', 'Back\TestimonialController@store')->name('testimonial.store');
Route::get('/admin/edit-testimonial/{testimonial}', 'Back\TestimonialController@edit')->name('testimonial.edit');
Route::patch('/admin/update-testimonial/{testimonial}', 'Back\TestimonialController@update')->name('testimonial.update');
Route::get('/admin/destroy-testimonial/{testimonial}', 'Back\TestimonialController@destroy')->name('testimonial.destroy');

//Blog
Route::get('/admin/create-blog', 'Back\BlogController@create')->name('blog.create');
Route::post('/admin/store-blog', 'Back\BlogController@store')->name('blog.store');
Route::get('/admin/edit-blog/{blog}', 'Back\BlogController@edit')->name('blog.edit');
Route::patch('/admin/update-blog/{blog}', 'Back\BlogController@update')->name('blog.update');
Route::get('/admin/destroy-blog/{blog}', 'Back\BlogController@destroy')->name('blog.destroy');


//Post
Route::get('/admin/create-post', 'Back\PostController@create')->name('post.create');
Route::post('/admin/store-post', 'Back\PostController@store')->name('post.store');
Route::get('/admin/edit-post/{post}', 'Back\PostController@edit')->name('post.edit');
Route::patch('/admin/update-post/{post}', 'Back\PostController@update')->name('post.update');
Route::get('/admin/destroy-post/{post}', 'Back\PostController@destroy')->name('post.destroy');



