<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;

class MessageController extends Controller
{
    public function store(Request $request)
    {
        $rule = [
            'name' => 'required',
            'email' => 'required|email',
            'website_name' => 'required',
            'message' => 'required'
        ];

        $this->validate($request, $rule);

        $message = new Message();
        $message->name = $request->name;
        $message->email = $request->email;
        $message->website_name = $request->website_name;
        $message->message = $request->message;
        $message->save();
        return redirect()->back()->with('success', 'Message Send Successfully!!!');
    }

    public function create()
    {
        $messages = Message::orderBy('updated_at', 'desc')->get();
        return view('back.message.index', compact('messages'));
    }

    public function destroy(Message $message)
    {
        $message->delete();
        return redirect()->back()->with('success', 'Message Deleted Successfully!!!');

    }
}
