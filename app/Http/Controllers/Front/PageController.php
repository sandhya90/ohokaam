<?php

namespace App\Http\Controllers\Front;

use App\Employer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function showIndex()
    {
        return view('front.pages.index');
    }

    public function showJob()
    {
        return view('front.pages.jobs');
    }

    
    public function showAbout()
    {
        return view('front.pages.about');
    }

    public function showBecomaprofessional()
    {
        return view('front.pages.becomaprofessional');
    }

    public function showBlog()
    {
        return view('front.pages.blog');
    }

    public function showBlogSingle()
    {
        return view('front.pages.blog-single');
    }

    public function showProfessionals()
    {
        return view('front.pages.professionals');
    }

    public function showSupport()
    {
        return view('front.pages.support');
    }

    public function showCategory()
    {
        return view('front.pages.category');
    }

    public function showProfile()
    {
        return view('front.pages.user');
    }

    public function showContact()
    {
        return view('front.pages.contact');
    }

    public function showMembers()
    {
        return view('front.pages.members');
    }

    public function showProtocol()
    {
        return view('front.pages.protocol');
    }

    public function showContactCandidate()
    {
        return view('front.pages.contact-candidate');
    }

    public function check()
    {
        return view('front.pages.professional');
    }

    public function showSearchedEmployer(Request $request)
    {
        
        $searchonemployer = $request->searchonemployer;
        $cat_id = $request->cat_id;

        $searched_employer = Employer::where('name', 'like', '%'.$searchonemployer.'%')
        ->where('maincatemployer_id', 'like', '%'.$cat_id.'%')
        ->get();
        return view('front.pages.searched_employer', compact('searched_employer', 'cat_id'));
     

        // if($request->get('searchonemployer')){
        //     $query = $request->get('searchonemployer');

        //     $searched_employer = Employer::where('name', 'like', '%'.$query.'%')->get();
        //    return view('front.pages.searched_employer', compact('searched_employer', 'query'));
        // }
        
        // return redirect()->back()->with('error', 'Your Searched employer not found!!!');
    }
}
