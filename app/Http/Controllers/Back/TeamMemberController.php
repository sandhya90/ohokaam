<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TeamMember;
use Illuminate\Support\Facades\Storage;

class TeamMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teamMembers = TeamMember::orderBy('updated_at', 'desc')->get();
        return view('back.team-member.index', compact('teamMembers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        $rules=[
            'name' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'post' => 'required',
        ];
       $validator = Validator($data,$rules);

       if ($validator->fails()){
           return redirect()
                       ->back()
                       ->withErrors($validator)
                      ->withInput();
       }else{
                  $image=$data['image'];
                  $input = time().'.'.$image->getClientOriginalExtension();
                  $destinationPath = 'files';
                  $image->move($destinationPath, $input);

        $teamMember = new TeamMember();
        $teamMember->name = $request->name;
        $teamMember->post = $request->post;
        $teamMember->facebook = $request->facebook;
        $teamMember->twitter = $request->twitter;
        $teamMember->google = $request->google;
        $teamMember->image = $input;
        $teamMember->save();
        return redirect()->back()->with('success', 'Team Member Stored Successfully!!!');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TeamMember $teamMember)
    {
        return view('back.team-member.edit', compact('teamMember'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TeamMember $teamMember)
    {

        $data=$request->all();
        $rules=[
            'name' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'post' => 'required',
        ];
       $validator = Validator($data,$rules);

       $teamMember->name = $request->name;
       $teamMember->post = $request->post;
       $teamMember->facebook = $request->facebook;
       $teamMember->twitter = $request->twitter;
       $teamMember->google = $request->google;
       $teamMember->image = $request->image;

       if($request->hasFile('image'))
       {
           $image=$data['image'];
           $input = time().'.'.$image->getClientOriginalExtension();
           $destinationPath = 'files';
           $image->move($destinationPath, $input);
           // Image::make($image)->resize(100,100)->save($input);
           $oldInput = $teamMember->image;
           $teamMember->image = $input;
           Storage::delete($oldInput);
       }


        $teamMember->save();
        return redirect()->route('teamMember.create')->with('success', 'Team Member Updated Successfully!!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeamMember $teamMember)
    {
        $teamMember->delete();
        return redirect()->back()->with('success', 'Team Member Deleted Successfully!!');
    }
}
