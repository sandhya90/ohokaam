<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Testimonial;
use Illuminate\Support\Facades\Storage;

class TestimonialController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $testimonials = Testimonial::orderBy('updated_at', 'desc')->get();
        return view('back.testimonial.index', compact('testimonials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        $rules=[
            'name' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'post' => 'required',
        ];
       $validator = Validator($data,$rules);

       if ($validator->fails()){
           return redirect()
                       ->back()
                       ->withErrors($validator)
                      ->withInput();
       }else{
                  $image=$data['image'];
                  $input = time().'.'.$image->getClientOriginalExtension();
                  $destinationPath = 'files';
                  $image->move($destinationPath, $input);

        $testimonial = new Testimonial();
        $testimonial->name = $request->name;
        $testimonial->post = $request->post;
        $testimonial->description = $request->description;
        $testimonial->image = $input;
        $testimonial->save();
        return redirect()->back()->with('success', 'Testimonial Stored Successfully!!!');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('back.testimonial.edit', compact('testimonial'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial $testimonial)
    {

        $data=$request->all();
        $rules=[
            'name' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'post' => 'required',
        ];
       $validator = Validator($data,$rules);

       $testimonial->name = $request->name;
       $testimonial->post = $request->post;
       $testimonial->description = $request->description;
       $testimonial->image = $request->image;

       if($request->hasFile('image'))
       {
           $image=$data['image'];
           $input = time().'.'.$image->getClientOriginalExtension();
           $destinationPath = 'files';
           $image->move($destinationPath, $input);
           // Image::make($image)->resize(100,100)->save($input);
           $oldInput = $testimonial->image;
           $testimonial->image = $input;
           Storage::delete($oldInput);
       }


        $testimonial->save();
        return redirect()->route('testimonial.create')->with('success', 'Testimonial Updated Successfully!!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        $testimonial->delete();
        return redirect()->back()->with('success', 'Testimonial Deleted Successfully!!');
    }

}
