<?php

namespace App\Http\Controllers\Back;

use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blogs = Blog::orderBy('updated_at', 'desc')->get();
        return view('back.blog.index', compact('blogs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        $rules=[
            'title' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'description' => 'required',
        ];
       $validator = Validator($data,$rules);

       if ($validator->fails()){
           return redirect()
                       ->back()
                       ->withErrors($validator)
                      ->withInput();
       }else{
                  $image=$data['image'];
                  $input = time().'.'.$image->getClientOriginalExtension();
                  $destinationPath = 'files';
                  $image->move($destinationPath, $input);

        $blog = new Blog();
        $blog->title = $request->title;
        $blog->date = $request->date;
        $blog->blogger_name = $request->blogger_name;
        $blog->description = $request->description;
        $blog->image = $input;
        $blog->save();
        return redirect()->back()->with('success', 'Blog Stored Successfully!!!');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        return view('back.blog.edit', compact('blog'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {

        $data=$request->all();
        $rules=[
            'title' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'description' => 'required',
        ];
       $validator = Validator($data,$rules);

       $blog->title = $request->title;
       $blog->date = $request->date;
       $blog->blogger_name = $request->blogger_name;
       $blog->description = $request->description;
       $blog->image = $request->image;

       if($request->hasFile('image'))
       {
           $image=$data['image'];
           $input = time().'.'.$image->getClientOriginalExtension();
           $destinationPath = 'files';
           $image->move($destinationPath, $input);
           // Image::make($image)->resize(100,100)->save($input);
           $oldInput = $blog->image;
           $blog->image = $input;
           Storage::delete($oldInput);
       }


        $blog->save();
        return redirect()->route('blog.create')->with('success', 'Blog Updated Successfully!!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $blog->delete();
        return redirect()->back()->with('success', 'Blog Deleted Successfully!!');
    }



}
