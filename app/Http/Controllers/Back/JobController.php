<?php

namespace App\Http\Controllers\Back;
use App\Job;
use App\MainCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $maincategories = MainCategory::orderBy('updated_at', 'desc')->get();
        $jobs = Job::orderBy('updated_at', 'desc')->get();
        return view('back.job.index', compact('maincategories', 'jobs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'no_of_vacancy' => 'required|numeric',
            'description' => 'required',
            'salary' => 'required|numeric',
            'type' => 'required',
            'level' => 'required',
            'education' => 'required',
            'skill' => 'required'
        ]; 
        $this->validate($request,$rule);

        $job = new Job();
        $job->maincategory_id = $request->maincategory_id;
        $job->subcategory_id = $request->subcategory_id;
        $job->no_of_vacancy = $request->no_of_vacancy;
        $job->description = $request->description;
        $job->salary = $request->salary;
        $job->type = $request->type;
        $job->level = $request->level;
        $job->education = $request->education;
        $job->skill = $request->skill;
        $job->save();
        return redirect()->back()->with('success', 'Job Stored Successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        return view('back.job.edit', compact('job'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {
        $rule = [
            'no_of_vacancy' => 'required|numeric',
            'description' => 'required',
            'salary' => 'required|numeric',
            'type' => 'required',
            'level' => 'required',
            'education' => 'required',
            'skill' => 'required'
        ]; 
        $this->validate($request,$rule);

        $job->subcategory_id = $request->subcategory_id;
        $job->no_of_vacancy = $request->no_of_vacancy;
        $job->description = $request->description;
        $job->salary = $request->salary;
        $job->type = $request->type;
        $job->level = $request->level;
        $job->education = $request->education;
        $job->skill = $request->skill;
        $job->save();
        return redirect()->route('job.create')->with('success', 'Job Stored Successfully!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        $job->delete();
        return redirect()->back()->with('success', 'Job Deleted Successfully!!');
    }

    public function showJob($id)
    {
        $jobs = Job::where('subcategory_id', $id)->orderBy('updated_at', 'desc')->get();
        return view('back.job.show', compact('jobs'));
        
    }
}
