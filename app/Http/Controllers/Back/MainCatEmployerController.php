<?php

namespace App\Http\Controllers\Back;
use App\MainCatEmployer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class MainCatEmployerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $maincatemployers = MainCatEmployer::orderBy('updated_at', 'desc')->get();
        return view('back.main-cat-employer.index', compact('maincatemployers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data=$request->all();
        $rules=[
            'name' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'location' => 'required',
        ];
       $validator = Validator($data,$rules);

       if ($validator->fails()){
           return redirect()
                       ->back()
                       ->withErrors($validator)
                      ->withInput();
       }else{
                  $image=$data['image'];
                  $input = time().'.'.$image->getClientOriginalExtension();
                  $destinationPath = 'files';
                  $image->move($destinationPath, $input);

        $maincatemployer = new MainCatEmployer();
        $maincatemployer->name = $request->name;
        $maincatemployer->location = $request->location;
        $maincatemployer->image = $input;
        $maincatemployer->save();
        return redirect()->back()->with('success', 'MainCategoryEmployer Stored Successfully!!!');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MainCatEmployer $maincatemployer)
    {
        return view('back.main-cat-employer.edit', compact('maincatemployer'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainCatEmployer $maincatemployer)
    {
        $data=$request->all();
        $rules=[
            'name' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'location' => 'required',
        ];
       $validator = Validator($data,$rules);

       $maincatemployer->name = $request->name;
       $maincatemployer->location = $request->location;
       $maincatemployer->image = $request->image;

       if($request->hasFile('image'))
       {
           $image=$data['image'];
           $input = time().'.'.$image->getClientOriginalExtension();
           $destinationPath = 'files';
           $image->move($destinationPath, $input);
           // Image::make($image)->resize(100,100)->save($input);
           $oldInput = $maincatemployer->image;
           $maincatemployer->image = $input;
           Storage::delete($oldInput);
       }


        $maincatemployer->save();
        return redirect()->route('maincatemployer.create')->with('success', 'MainCategoryEmployer Updated Successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainCatEmployer $maincatemployer)
    {
        $maincatemployer->delete();
        return redirect()->back()->with('success', 'MainCategory Deleted Successfully!!');
    }

    
}
