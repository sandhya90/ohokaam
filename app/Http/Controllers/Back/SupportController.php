<?php

namespace App\Http\Controllers\Back;
use App\Support;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class supportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supports = Support::orderBy('updated_at', 'desc')->get();
        return view('back.support.index', compact('supports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'question' => 'required',
            'answer' => 'required',
            
        ]; 
        $this->validate($request,$rule);

        $support = new Support();
        $support->question = $request->question;
        $support->answer = $request->answer;
        $support->save();
        return redirect()->back()->with('success', 'Support Stored Successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Support $support)
    {
        return view('back.support.edit', compact('support'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Support $support)
    {
        $rule = [

            'question' => 'required',
            'answer' => 'required'
        ]; 
        $this->validate($request,$rule);

        $support->question = $request->question;
        $support->answer = $request->answer;
        $support->save();
        return redirect()->route('support.create')->with('success', 'support Stored Successfully!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Support $support)
    {
        $support->delete();
        return redirect()->back()->with('success', 'Support Deleted Successfully!!');
    }

    public function showSearchedItem(Request $request)
    {
        if($request->get('searchonquestion')){
            $query = $request->get('searchonquestion');

            $searched_questions = Support::where('question', 'like', '%'.$query.'%')->get();
           return view('front.pages.searched-item', compact('searched_questions'));
        }
        
    }
}
