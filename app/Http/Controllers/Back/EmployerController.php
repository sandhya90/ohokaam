<?php

namespace App\Http\Controllers\Back;
use App\Employer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MainCatEmployer;
use App\Post;

class EmployerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $posts = Post::orderBy('updated_at', 'desc')->get();
        $maincatemployers = MainCatEmployer::orderBy('updated_at', 'desc')->get();
        $employers = Employer::orderBy('updated_at', 'desc')->get();
        return view('back.employer.index', compact('posts', 'maincatemployers', 'employers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'no_of_jobs' => 'required|numeric',
            'maincatemployer_id' => 'required',
            'post_id' => 'required',
            'type' => 'required',
            'deadline' => 'required',
            'level' => 'required',
            'salary' => 'required|numeric',
            'no_of_vacancy' => 'required|numeric',
            'skill' => 'required',
            'experience' => 'required',
            'description' => 'required',
        ]; 
        $this->validate($request,$rule);

       
    
        $arrayToString = implode(',', $request->input('post_id'));
        $inputValue['post_id'] = $arrayToString;
        
        $employer = new Employer();
        $employer->maincatemployer_id = $request->maincatemployer_id;
        
        $employer->post_id = $inputValue['post_id'];
        // dd($request->post_id);

        // $post_id = $request->post_id;

        // if(is_array($post_id))
        // {
        //     $post_id = implode(',', $post_id);
        //     $employer->post_id = $post_id;           
        // }

        $employer->no_of_jobs = $request->no_of_jobs;
        $employer->type = $request->type;
        $employer->deadline = $request->deadline;
        $employer->level = $request->level;
        $employer->salary = $request->salary;
        $employer->no_of_vacancy = $request->no_of_vacancy;
        $employer->skill = $request->skill;
        $employer->experience = $request->experience;
        $employer->description = $request->description;
        $employer->save();
        return redirect()->back()->with('success', 'Job Stored Successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    















    public function employersCat(Request $request)
    {   
        $cat_id = $request->cat_id;
        $type = $request->type;
        $level = $request->level;

        if($cat_id !="" && $type !="" && $level !=""){
            $data = Employer::where('maincatemployer_id', $cat_id)
                    ->where('type', $type)
                    ->where('level', $level)
                    ->get();

        }else if($type !="" && $level !="") {
            $data = Employer::where('type', $type)
                    ->where('level', $level)
                    ->get();  

        }else if($level !=""){
            $data = Employer::where('level', $level)->get();                                
        }

        if(count($data) == "0"){
            echo "<h2 align = 'center' style = 'color:red'>No Employer Available here!!!</h2>";
        }else{
            return view('front.pages.filteredEmployer', compact('data'));

        }


    }
}
