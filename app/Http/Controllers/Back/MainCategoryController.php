<?php

namespace App\Http\Controllers\Back;
use App\MainCategory;
use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $maincategories = MainCategory::orderBy('updated_at', 'desc')->get();
        return view('back.main-category.index', compact('maincategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'name' => 'required',
        ]; 
        $this->validate($request,$rule);

        $maincategory = new MainCategory();
        $maincategory->name = $request->name;
        $maincategory->save();
        return redirect()->back()->with('success', 'MainCategory Stored Successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MainCategory $maincategory)
    {
        return view('back.main-category.edit', compact('maincategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainCategory $maincategory)
    {

        $rule = [
            'name' => 'required',
        ]; 
        $this->validate($request,$rule);

        $maincategory->name = $request->name;
        $maincategory->save();
        return redirect()->route('maincategory.create')->with('success', 'MainCategory Updated Successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainCategory $maincategory)
    {
        $maincategory->delete();
        return redirect()->back()->with('success', 'MainCategory Deleted Successfully!!');
    }
    

    public function getSubCategory($maincategory_id)
    {
        $subcategory = SubCategory::where('maincategory_id', $maincategory_id)->get();
        return $subcategory;
    }
}
