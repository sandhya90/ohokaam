<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
    public function subCategory()
    {
        return $this->hasMany('App\SubCategory');
    }

    public function job()
    {
        return $this->hasMany('App\Job');
    }
}
