<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function employer()
    {
        return $this->hasMany('App\Employer');
    }
}
