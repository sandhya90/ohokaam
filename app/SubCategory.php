<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{

    public function mainCategory()
    {
        return $this->belongsTo('App\MainCategory', 'maincategory_id');
    }

    public function job()
    {
        return $this->hasMany('App\Job');
    }

}
