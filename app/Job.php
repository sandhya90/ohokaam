<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public function subCategory()
    {
        return $this->belongsTo('App\SubCategory', 'subcategory_id');
    }

    public function mainCategory()
    {
        return $this->belongsTo('App\MainCategory', 'maincategory_id');
    }
}
