<?php

namespace App\Providers;

use App\Blog;
use App\Employer;
use App\Job;
use App\MainCategory;
use App\SubCategory;
use App\TeamMember;
use App\Testimonial;
use App\MainCatEmployer;
use Illuminate\Support\ServiceProvider;

class ViewIndexProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('front.pages.index', function($view){
            $maincategories = MainCategory::orderBy('updated_at', 'desc')->get();
            $maincatemployers = MainCatEmployer::orderBy('updated_at', 'desc')->get();
            $subcategories = SubCategory::orderBy('updated_at', 'desc')->get();
            $view->with(compact(['maincategories', 'subcategories', 'maincatemployers']));
        });

        view()->composer('front.pages.category', function($view){
            $jobs = Job::orderBy('updated_at', 'desc')->get();
            $view->with(compact('jobs'));
        });

        view()->composer('front.pages.about', function($view){
            $team_members = TeamMember::orderBy('updated_at', 'desc')->paginate(3);
            $testimonials = Testimonial::orderBy('updated_at', 'desc')->get();
            $view->with(compact(['team_members', 'testimonials']));
        });

        view()->composer('front.pages.members', function($view){
            $team_members = TeamMember::orderBy('updated_at', 'desc')->paginate(6);
            $view->with(compact('team_members'))->with('i', (request()->input('page', 1) -1) * 6);
        });

        view()->composer('front.pages.blog-single', function($view){
            $blogs = Blog::orderBy('updated_at', 'desc')->paginate(6);
            $view->with(compact('blogs'))->with('i', (request()->input('page', 1) -1) * 6);
        });

        view()->composer('front.pages.blog', function($view){
            $blogs = Blog::orderBy('updated_at', 'desc')->get();
            $view->with(compact('blogs'));
        });

        view()->composer('front.pages.jobs', function($view){
            $maincatemployers = MainCatEmployer::orderBy('updated_at', 'desc')->get();
            $employers = Employer::orderBy('updated_at', 'desc')->get();
            $view->with(compact(['employers', 'maincatemployers']));
        });



    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
