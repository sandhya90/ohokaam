<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('no_of_jobs');
            $table->string('type');
            $table->string('deadline');
            $table->string('level');
            $table->decimal('salary');
            $table->integer('no_of_vacancy');
            $table->string('skill');
            $table->string('experience');
            $table->text('description');
            $table->unsignedBigInteger('maincatemployer_id');
            $table->foreign('maincatemployer_id')->references('id')->on('main_cat_employers')->onDelete('cascade');
            $table->unsignedBigInteger('post_id');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employers');
    }
}
