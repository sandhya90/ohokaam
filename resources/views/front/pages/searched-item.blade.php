<!DOCTYPE html>
<html class="no-js" lang="">
  <head>
    @include('front.include.meta')

      <title>HR|Heavenmaker</title>

      @include('front.include.head')

     <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  </head>

  <body class="support">
    <!-- model start-->
     <!-- Extra Large modal -->
     @include('front.include.model')

    <!-- model end -->
    <!--nav button start -->
    @include('front.include.mobile-nav')

    <!-- nav button end -->
    <!-- header  start-->
    @include('front.include.header')

    <!-- banner start-->
    <section class="banner-page">
      <div class="container-fluid">
        <div class="row">
         <div class="col-1-of-1">
           <h3 >Support</h3>
           <div class="breadcrumb">
             <ul>
               <li>
                 <a href="./">Home</a>
               </li>
               <li>
                 <a href="{{route('support')}}" class="active">Support</a>
               </li>
             </ul>
           </div>
         </div>
        </div>
      </div>
    </section>
    <!-- banner end -->

@foreach($searched_questions as $searched_question)
    <div class="Query-list">
                            <div class="accordion">
                                <div class="accordion-item">
                                <a>{{$searched_question->question}}</a>
                                <div class="content">
                                    <p>{{$searched_question->answer}}</p>
                                </div>
                                </div>
                                
                            </div>
                   </div>
@endforeach

<div class="helpful clearfix">
                       <p>Wasn't this answer helpful?</p>
                       <a href="{{route('contact')}}">Contact Us<i class="fas fa-chevron-right"></i></a>
                   </div>

    
    @include('front.include.footer')

   <!-- footer end -->
    <!-- script start -->
    @include('front.include.script')
    <script>
        const items = document.querySelectorAll(".accordion a");

function toggleAccordion(){
  this.classList.toggle('active');
  this.nextElementSibling.classList.toggle('active');
}

items.forEach(item => item.addEventListener('click', toggleAccordion));
        </script>
    <!-- script end -->
  </body>
</html>