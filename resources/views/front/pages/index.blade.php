<!DOCTYPE html>
<html class="no-js" lang="">
  <head>
  @include('front.include.meta')

      <title>HR|Heavenmaker</title>
      <link rel="stylesheet" type="text/css" href="css/vendors/style2.css" />
      <script type="text/javascript" src="js/vendors/modernizr.custom.28468.js"></script>
      <noscript>
      <link rel="stylesheet" type="text/css" href="css/nojs.css" />
      </noscript>
      @include('front.include.head')
  </head>

  <body class="index">
    <!-- model start-->
     <!-- Extra Large modal -->
     @include('front.include.model')

    <!-- model end -->
    <!--nav button start -->
    @include('front.include.mobile-nav')

    <!-- nav button end -->
    <!-- header  start-->
    @include('front.include.header')

    <!-- header end -->
    <!-- Banner start -->
    <section class="banner">
        <div class="banner-wrap clearfix">
          <h2>The Biggest Marketplace for Digital Goods</h2>
          <p>DigitalMarket is a powerful Digital Marketplace theme to Buy and Sell</p>
          <form action="{{url('employer/search')}}" method="post" class="clearfix">
          @csrf()
              <div class="banner-form dropdown">
                  <div class="md-select">
                     <button type="button" class="ng-binding dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category</button>
                      <ul role="listbox" id="ul-id" class="md-whiteframe-z1" aria-activedescendant="state2_AK" name="ul-id">
                        @foreach($maincatemployers as $maincatemployer)
                        <li name='cat_id' role="option" id="state2_AK" class="ng-binding ng-scope active" tabindex="-1" aria-selected="true" value="{{$maincatemployer->id}}">{{$maincatemployer->name}}</li>                        
                        @endforeach
                      </ul>
                    </div>                   
              </div>
              <div class="search-service ">
                <div class="search-item">
                    <input type="text" name="searchonemployer" placeholder="Search for a service.." value="">
                    <button type="submit" class="site-btn">SEARCH</button>

                    <!-- <button type="submit"><span class="icon_search"></span></button> -->
                  <!-- <label for="search"><i class="fas fa-search"></i></label> -->
                </div>
                <p>E.g. Packers and Movers, Handyman, Maid Service</p>
              </div>
          </form>
        </div>
        <ul>
          <li>
            <a href="" class="category1 category"><img src="img/thumb-4.jpg" alt="category">
            </a>
          </li>
          <li>
            <a href="" class="category2 category"><img src="img/thumb.jpg" alt="category"></a>
          </li>
          <li>
            <a href="" class="category3  category"><img src="img/thumb-1-3.jpg" alt="category"></a>
          </li>
          <li>

            <a href="" class="category4  category"><img src="img/thumb-1-2.jpg" alt="category"></a> 
          </li>
          <li>

            <a href="" class="category5  category"><img src="img/thumbnail-10.jpg" alt="category"></a>  
          </li>
          <li>
            <a href="" class="category6 category"><img src="img/thumbnail-9.png" alt="category"></a>

          </li>
          <li>

            <a href="" class="category7 category"><img src="img/thumb-1-1.jpg" alt="category"></a>
          </li>
        </ul>  
    </section>
    <!-- banner end -->
    <!-- Jobs Category Start -->
    <section class="jobs-category">
      <div class="container-fluid">
        <h3 class="main--header">Find jobs by Category</h3>
        <div class="row">
        @foreach($maincategories as $maincategory)
          <div class="col-1-of-3">
              <div class="jobs-category-item">
                <div class="jobs-category-item-images">
                    <a href="" class="jobs-category-item--img">
                      <img src="img/category/ic-dust-cleaner.svg" alt="item">
                    </a>
                    <h4>{{$maincategory->name}}</h4>                
                </div>
                @foreach($subcategories as $subcategory)

                <div class="jobs-category-item-sub">
                  <div class="jobs-category-item-subitem">
                    <a href="{{url('admin/subcategory', $subcategory->id)}}">{{$subcategory->name}}</a>
                  </div>
                </div>
                @endforeach

              </div>
          </div>
          @endforeach


        </div>

        <a href="{{route('category')}}" class="btn--category">Browse All Categories</a>
      </div>
    </section>
    <!-- jobs Category End -->
    <!-- join with team start -->
    <section class="join-team">
      <div class="container-fluid">
        <div class="row">
          <div class="col-4-of-3">
              <div class="join-team-wrap">
                  <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti molestiae</h3>
                  
              </div>
          </div>
          <div class="col-4-of-1">
              <a href="" class="btn--category">Become A Professional</a>
            </div>
        </div>
      </div>
    </section>
    <!-- join with team end -->
    <!-- Featured Jobs start -->
    <section class="jobs-employee">
      <div class="container-fluid">
        <div class="row">
            <div class="col-1-of-2 featured-jobs">
                <h4 class="main--header">Featured jobs</h4>
                <div class="featured-jobs-list">
                  <a class="featured-jobs-list-item active">
                    <div class="row">
                      <div class="col-1-of-2 clearfix">
                        <div class="image">
                          <img src="img/06.png" alt="img">
                        </div>
                        <div class="content">
                            <h5>IT Web Developer</h5>
                            <p>Expedia</p>
                          </div>
                      </div>
                      <div class="col-1-of-3 location">
                        <p><i class="fa fa-map-marker text-primary"></i> Dallu,Ktm</p>
                      </div>
                      <div class="col-1-of-4 _btn">
                        <button class="btn--success btn--featured ">Freelance</button>
                        <span>1 day ago</span>
                      </div>
                    </div>
                  </a>
                  <a class="featured-jobs-list-item">
                      <div class="row">
                        <div class="col-1-of-2 clearfix">
                          <div class="image">
                            <img src="img/05.png" alt="img">
                          </div>
                          <div class="content">
                              <h5>IT Web Developer</h5>
                              <p>Expedia</p>
                            </div>
                        </div>
                        <div class="col-1-of-3 location">
                          <p><i class="fa fa-map-marker text-primary"></i> Dallu,Ktm</p>
                        </div>
                        <div class="col-1-of-4 _btn">
                          <button class="btn--danger btn--featured ">Part-time</button>
                          <span>1 day ago</span>
                        </div>
                      </div>
                  </a>
                  <a class="featured-jobs-list-item">
                      <div class="row">
                        <div class="col-1-of-2 clearfix">
                          <div class="image">
                            <img src="img/04.png" alt="img">
                          </div>
                          <div class="content">
                              <h5>IT Web Developer</h5>
                              <p>Expedia</p>
                            </div>
                        </div>
                        <div class="col-1-of-3 location">
                          <p><i class="fa fa-map-marker text-primary"></i> Dallu,Ktm</p>
                        </div>
                        <div class="col-1-of-4 _btn">
                          <button class="btn--success btn--featured ">Freelance</button>
                          <span>1 day ago</span>
                        </div>
                      </div>
                  </a>
                  <a class="featured-jobs-list-item">
                      <div class="row">
                        <div class="col-1-of-2 clearfix">
                          <div class="image">
                            <img src="img/01.png" alt="img">
                          </div>
                          <div class="content">
                              <h5>IT Web Developer</h5>
                              <p>Expedia</p>
                            </div>
                        </div>
                        <div class="col-1-of-3 location">
                          <p><i class="fa fa-map-marker text-primary"></i> Dallu,Ktm</p>
                        </div>
                        <div class="col-1-of-4 _btn">
                          <button class="btn--success btn--featured ">Freelance</button>
                          <span>1 day ago</span>
                        </div>
                      </div>
                  </a>
                  <a class="featured-jobs-list-item">
                      <div class="row">
                        <div class="col-1-of-2 clearfix">
                          <div class="image">
                            <img src="img/02.png" alt="img">
                          </div>
                          <div class="content">
                              <h5>IT Web Developer</h5>
                              <p>Expedia</p>
                            </div>
                        </div>
                        <div class="col-1-of-3 location">
                          <p><i class="fa fa-map-marker text-primary"></i> Dallu,Ktm</p>
                        </div>
                        <div class="col-1-of-4 _btn">
                          <button class="btn--warning btn--featured ">Full-time</button>
                          <span>1 day ago</span>
                        </div>
                      </div>
                  </a>
                </div>
            </div>
            <div class="col-1-of-2 top-employees">
              <h4 class="main--header">
                  top employers
              </h4>
              <div class="top-employees-list">
                <div class="top-employees-list-item">
                  <div class="image">
                    <img src="img/01.png" alt="img">
                  </div>
                  <p>Freelancer</p>
                  <a href="">view 15 jobs</a>
                </div>
                <div class="top-employees-list-item">
                    <div class="image">
                      <img src="img/05.png" alt="img">
                    </div>
                    <p>Wotif</p>
                    <a href="">view 15 jobs</a>
                  </div>
                  <div class="top-employees-list-item">
                  <div class="image">
                    <img src="img/18.png" alt="img">
                  </div>
                  <p>Cisco</p>
                  <a href="">view 15 jobs</a>
                </div>
                <div class="top-employees-list-item">
                    <div class="image">
                      <img src="img/12.png" alt="img">
                    </div>
                    <p>Airbnb</p>
                    <a href="">view 15 jobs</a>
                  </div>
                  <div class="top-employees-list-item">
                      <div class="image">
                        <img src="img/10.png" alt="img">
                      </div>
                      <p>Merck</p>
                      <a href="">view 15 jobs</a>
                    </div>
                    <div class="top-employees-list-item">
                        <div class="image">
                          <img src="img/08.png" alt="img">
                        </div>
                        <p>FedEx</p>
                        <a href="">view 15 jobs</a>
                      </div>
              </div>
            </div>
        </div>
        <div class="text__center">
          <a href="{{route('job')}}" class="btn--category">Browse All Jobs</a>
        </div>
      </div>
    </section>
    <!-- Featured Jobs End -->
    <!-- mobile start -->
    <section class="mobile">
      <div class="container-fluid">
          <div class="row">
            <div class="col-1-of-2">
              <div class="mobile-img">
                 <div class="mobile-content">
                 <div class="join-team-wrap">
                  <div class="join-team-wrap-item">
                  <div id="da-slider" class="da-slider">
                        <div class="da-slide">
                        <div class="da-img"><img src="img/mobile/slider/1.png" alt="image01" /></div>
                          <h2>Bollywood Dance <br> Instructor</h2>
                          <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                          <a href="professional.php" class="da-link">Aastha Sharma</a>
                          
                        </div>
                        <div class="da-slide">
                          <div class="da-img"><img src="img/mobile/slider/3.png" alt="image01" /></div>
                          <h2>Make-up Artist</h2>
                          <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                          <a href="professional.php" class="da-link">Natasha Arya</a>
                        </div>
                        <div class="da-slide">
                          <div class="da-img"><img src="img/mobile/slider/1.png" alt="image01" /></div>
                          <h2>Wedding <br>  Photographer</h2>
                          <p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.</p>
                          <a href="professional.php" class="da-link">Sajal Kapoor</a>
                        </div>
                        <div class="da-slide">
                          <div class="da-img"><img src="img/mobile/slider/2.png" alt="image01" /></div>
                          <h2>Fitness Trainer</h2>
                          <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
                          <a href="professional.php" class="da-link">Yasser Raja</a>
                        </div>
                      </div>
                  </div>
              </div>
                 </div>
              </div>
            </div>
            <div class="col-1-of-2">
              <div class="download">
                <div class="download-app">
                  <h3 class="main--header">
                    download the App
                  </h3>
                  <p>Choose and book from 100+s\ services and track on the go with the Hr-Heavenmaker App</p>
                  <form action="">
                    <strong>If you have any issues on app.</strong>
                    <textarea rows="1" placeholder="Message"></textarea>
                    <input type="submit" value="Send" class="join--btn">
                  </form>
                  <div class="download-link">
                    <a href="">
                      <img src="img\mobile\appStore.png" alt="apple">
                    </a>
                    <a href="">
                      <img src="img\mobile\googlePlay.png" alt="android">
                    </a>
                  </div>
                </div>
              </div>
            </div>

          </div>
      </div>
    </section>
    <!-- mobile end -->
    <!-- footer start -->

    
    @include('front.include.footer')

   <!-- footer end -->
    <!-- script start -->
    @include('front.include.script')

      <script type="text/javascript" src="{{asset('js/vendors//jquery.cslider.js')}}"></script>
    <script type="text/javascript">
			$(function() {
			
				$('#da-slider').cslider({
					autoplay	: true,
					bgincrement	: 450
				});
			
			});
		</script>
    <!-- script end -->
  </body>
</html>
