<!DOCTYPE html>
<html class="no-js" lang="">
  <head>
  @include('front.include.meta')

      <title>HR|Heavenmaker</title>
      
      @include('front.include.head')

  </head>

  <body class="contact">
    <!-- model start-->
     <!-- Extra Large modal -->
     @include('front.include.model')

    <!-- model end -->
    <!--nav button start -->
    @include('front.include.mobile-nav')

    <!-- nav button end -->
    <!-- header  start-->
    @include('front.include.header')

    <!-- header end -->
    <!-- banner start-->
    <section class="banner-page">
      <div class="container-fluid">
        <div class="row">
         <div class="col-1-of-1">
           <h3>Contact</h3>
           <div class="breadcrumb">
             <ul>
               <li>
                 <a href="./">Home</a>
               </li>
               <li>
                 <a href="" class="active"><span>&#47</span>Contact</a>
               </li>
             </ul>
           </div>
         </div>
        </div>
      </div>
    </section>
    <!-- banner end -->
    <!-- contact start -->
    <section class="contact-page">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12-of-8">
            <div class="contact-page-map">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7065.462890258575!2d85.33544097711862!3d27.694694030910046!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb199a06c2eaf9%3A0xc5670a9173e161de!2sNew+Baneshwor%2C+Kathmandu+44600!5e0!3m2!1sen!2snp!4v1564637693277!5m2!1sen!2snp" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="contact-page-cantent clearfix">
            <div class="address-left">
                <h3>Get in <span>Touch</span></h3>
                <p>Visit our agency or simply send us an email anytime you want. If you have any questions, please feel free to contact us.</p>
              </div>
              <div class="address-left">
                <h5>Headquarters</h5>
                <address>
                  121 King Street, Melbourne <br>
                  Victoria 3000 Australia <br>
                  Envato Pty Ltd
                </address>
                <p>
                  Phone: <a href="tel:+11234567890">+1 123-456-7890</a> <br>
                  Email: <a href="mailto:email@example.com">email@example.com</a>
                </p>
              </div>
              <div class="address-left">
                <h5>Secondary Office</h5>
                <address>
                  47 Queen Street, Melbourne <br>
                  Victoria 3000 Australia <br>
                  Envato Pty Ltd
                </address>
                <p>
                  Phone: <a href="tel:+11234567890">+1 123-456-7890</a> <br>
                  Email: <a href="mailto:email@example.com">email@example.com</a>
                </p>
              </div>
            </div>
          </div>
          <div class="col-12-of-4">
            <div class="widget-form">
              <h5>Send Us a Message</h5>
              <div class="widget-content">
                <p>Consectetur adipisicing elit. Incidunt odio in sed velit quod. Unde dolores minima quisquam libero veritatis consequuntur accusamus nemo.</p>
                <form action="{{route('message.store')}}" method="post" class="clearfix">
                  @csrf()
                  <input type="string" name="name" placeholder="Name">
                  <input type="email" name="email" placeholder="Email">
                  <input type="string" name="website_name"  placeholder="Website">
                  <textarea name="message" class="form-control" rows="3" placeholder="How can we help you?"></textarea>
                  <button type="submit" class="join--btn"><i class="fa fa-envelope-o"></i> Send Message</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- contact end -->
    <!-- footer start -->
    @include('front.include.footer')

   <!-- footer end -->
    <!-- script start -->
    @include('front.include.script')
    <!-- script end -->
  </body>
</html>
