<!DOCTYPE html>
<html class="no-js" lang="">
  <head>
  @include('front.include.meta')

      <title>HR|Heavenmaker</title>
      <!-- Owl Carousel start -->
      <link rel="stylesheet" href="{{asset('css\vendors\owl.carousel.min.css')}}">
      <link rel="stylesheet" href="{{asset('css\vendors\owl.theme.default.min.css')}}">
      <!-- Owl Catousel end -->
      @include('front.include.head')

     
  </head>
  <!-- facebook share -->
  <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0&appId=1117190871802160&autoLogAppEvents=1"></script>

  <body class="blog">
    <!-- model start-->
     <!-- Extra Large modal -->
     @include('front.include.model')

    <!-- model end -->
    <!--nav button start -->
    @include('front.include.mobile-nav')

    <!-- nav button end -->
    <!-- header  start-->
    @include('front.include.header')

    <!-- header end -->
    <!-- blog-search start -->
    <section class="blog-details">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12-of-8 mobile--bottom">
                    <div class="breadcrumb">
                        <ul>
                            <li>
                                <a href="./">Home</a>
                            </li>
                            <li>
                                <a href="./blog.php"><span>/</span>Blog</a>
                            </li>
                            <li>
                                <a href="" class="active"><span>/</span>Blog-Details</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        @foreach($blogs as $blog)
                        <div class="col-1-of-1 ">
                            <div class="blog-details__image">
                                <img src="files/{{$blog->image}}" alt="image">
                                <a href=""  class="blog--cat"><i class="fas fa-thumbs-up"></i> 10 Likes</a>
                            </div>
                            <div class="post__header">
                                <h4 class="title">
                                    {{$blog->title}}
                                </h4>
                                <p class="meta--post">
                                    Posted on {{$blog->date}} by <a href=''>{{$blog->blogger_name}}</a> in <a href=''>Comuter</a>, <a href=''>Future</a> with <a href=''>6 Comments</a>
                                </p>
                            </div>

                            <p class="post--content">{{$blog->description}}
                            </p>
                            <div class="ads--details">
                                <img src="img/blog/ads--details.jpg" alt="ads">
                            </div>
                            <div class="post-tags"><span class="strong">Tags:</span> <a href="" rel="tag">Thoughts</a>, <a href="" rel="tag">Tips</a></div>
                            <div class="social--share">
                                <div class="share">
                                    <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                                </div>
                                <div class="share">
                                    <script src="https://platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
                                    <script type="IN/Share" data-url="https://www.linkedin.com"></script>
                                </div>
                                <div class="share">
                                <script src="https://apis.google.com/js/platform.js" async defer></script>
                                    <g:plus action="share"></g:plus>
                                </div>
                            </div>
                            <div class="user-professional">
                                <div class="comment-list">
                                    <ul class="clearfix">
                                        <li>
                                            <a href="" class="user">
                                                <img src="img\applicant\face-2.png" alt="cmt">
                                            </a>
                                        <div class="comment">
                                                <p><a href="">Francisco Da Silva</a><span>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ipsam molestiae</span></p>
                                                <div class="reply-link">
                                                    <div class=" login-trigger"><div class="reply">reply</div><small>-35m</small></div>
                                                    <form class="login-content">
                                                    <div class="content">
                                                    <a href="" class="user">
                                                            <img src="img\applicant\face-3.png" alt="cmt">
                                                        </a>
                                                        <textarea name="textare" rows="1" placeholder="Write a reply"></textarea>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="" class="user">
                                                <img src="img\applicant\face-2.png" alt="cmt">
                                            </a>
                                        <div class="comment">
                                                <p><a href="">Francisco Da Silva</a><span>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ipsam molestiae</span></p>
                                                <div class="reply-link">
                                                    <div class=" login-trigger"><div class="reply">reply</div><small>-35m</small></div>
                                                    <form class="login-content">
                                                    <div class="content">
                                                    <a href="" class="user">
                                                            <img src="img\applicant\face-3.png" alt="cmt">
                                                        </a>
                                                        <textarea name="textare" rows="1" placeholder="Write a reply"></textarea>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="" class="user">
                                                <img src="img\applicant\face-2.png" alt="cmt">
                                            </a>
                                        <div class="comment">
                                                <p><a href="">Francisco Da Silva</a><span>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ipsam molestiae</span></p>
                                                <div class="reply-link">
                                                    <div class=" login-trigger"><div class="reply">reply</div><small>-35m</small></div>
                                                    <form class="login-content">
                                                    <div class="content">
                                                    <a href="" class="user">
                                                            <img src="img\applicant\face-3.png" alt="cmt">
                                                        </a>
                                                        <textarea name="textare" rows="1" placeholder="Write a reply"></textarea>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
                                        <div class="toogle-details">
                                            <a class="candidat-toggle candidat-toggle--name"><i class="fas fa-chevron-down"></i><span>Show More</span></a>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                            <div class="comment-post">
                                <h4 class="main--header">Comments On Post</h4>
                                <p class="comment__warning error">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam, maxime laudantium *
                                </p>
                                <form action="" class="comment-on-post ">
                                    <div class="col-1-of-1">
                                        <div class="contract__field clearfix row">
                                            <fieldset>
                                                <p class="input__text">Your Comment</p>
                                                <textarea class="comment__area" name="" id=""  rows="4" placeholder="Your Comments"></textarea>
                                            </fieldset>
                                            
                                        </div>
                                    </div>
                                    <fieldset class="submit--btn">
                                        <input type="submit" class="join--btn" value="Submit">
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        @endforeach
                   </div>
                </div>
                <div class="col-12-of-4">
                    <form action="" class="post__search">
                        <input type="text" placeholder="Search ..." id="blog-post-search">
                        <label for="blog-post-search">
                            <i class="fas fa-search"></i>
                        </label>
                    </form>
                    <h4 class="main--header">Lests Get Social</h4>
                    <p class="blog-search--content">
                        Quis nostrum exercitationem ullam corporis suscipit laboriosam molestiae consequatur
                    </p>
                    <div class="share-user">
                        <div class="apply-share">
                            <ul>
                                <li class="facebook-color social"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="twitt-color social"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li class="pinterest-color social"><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <h4 class="main--header">Related Posts</h4>    
                    <div class="service-list clearfix">
                        <div class=" u-padding-left">
                            <div class="service-wrap">
                                <div class="service-carousel owl-carousel owl-theme">
                                    <div>
                                        <a class="item" href="">
                                            <img src="img/service/react-01.png" alt="service">
                                        </a>
                                        <div class="wrap-content clearfix">
                                        <a href="" class="wrap-content-mid">
                                        <figure>
                                            <img src="img/applicant/face-3.png" alt="user">
                                        </figure>
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint in ullam corrupti ducimus adipisci quas rem voluptatibus quia deserunt..</p>   
                                            </a>
                                        </div>
                                        <h5>Web developer Needed</h5>
                                        <div class="freelancers-rating">
                                            <ul class="clearfix ">
                                                <li><span class="likes"><i class="fas fa-thumbs-up"></i>10 </span></li>
                                                <li><span class='comments'><i class="fas fa-comment"></i>15 </span></li>
                                            </ul>
                                        </div>
                                        </div>
                                        <div>
                                            <a class="item" href="">
                                                <img src="img/service/react-01.png" alt="service">
                                            </a>
                                            <div class="wrap-content clearfix">
                                            <a href="" class="wrap-content-mid">
                                            <figure>
                                                <img src="img/applicant/face-3.png" alt="user">
                                            </figure>
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint in ullam corrupti ducimus adipisci quas rem voluptatibus quia deserunt..</p>   
                                                </a>
                                            </div>
                                            <h5>Web developer Needed</h5>
                                            <div class="freelancers-rating">
                                                <ul class="clearfix">
                                                <li><span class="likes"><i class="fas fa-thumbs-up"></i>5 </span></li>
                                                    <li><span class='comments'><i class="fas fa-comment"></i>8 </span></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div>
                                            <a class="item" href="">
                                                <img src="img/service/react-01.png" alt="service">
                                            </a>
                                            <div class="wrap-content clearfix">
                                            <a href="" class="wrap-content-mid">
                                            <figure>
                                                <img src="img/applicant/face-3.png" alt="user">
                                            </figure>
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint in ullam corrupti ducimus adipisci quas rem voluptatibus quia deserunt..</p>   
                                                </a>
                                            </div>
                                            <h5>Web developer Needed</h5>
                                            <div class="freelancers-rating">
                                                <ul class="clearfix">
                                                    <li><span class="likes"><i class="fas fa-thumbs-up"></i>9 </span></li>
                                                    <li><span class='comments'><i class="fas fa-comment"></i>10 </span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>  
                    <div class="sticky">
                    <h4 class="main--header">Tags</h4>
                    <div class="addtional">
                        <ul>
                          <li>
                            <a href=''>Home Service</a>
                          </li>
                          <li>
                            <a href=''>Php Developer</a>
                          </li>
                          <li>
                            <a href=''>Full Stack</a>
                          </li>
                          <li>
                            <a href=''>Magento</a>
                          </li>
                          <li>
                            <a href=''>UI &amp; Ux Designer</a>
                          </li>
                          
                        </ul>
                    </div>
                    <div class="ads">
                        <a href="" class="ads--image">
                        <img src="http://absolute.cactusthemes.com/wp-content/uploads/2014/11/Absolute-banner-002.jpg">
                        </a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- blog-search end -->
    <!-- footer start -->
    @include('front.include.footer')

   <!-- footer end -->
    <!-- script start -->
    @include('front.include.script')

     <script src="{{asset('js\vendors\owl.carousel.min.js')}}"></script>
    <script>
        $('.service-carousel').owlCarousel({
            loop:true,
            margin:0,
            nav:true,
            navText:['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
            dots:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        })
    </script>
    <!-- script end -->
  </body>
</html>
