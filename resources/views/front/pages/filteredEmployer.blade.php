
<div class="candidate-description clearfix">
              @foreach($data as $employer)
              <div class="candidate-description-image">
                <picture>
                  <img src="img/01.png" alt="img">
                </picture>
              </div>
              <div class="candidate-description-content clearfix">
                <div class="header">
                  <div class="header-left">
                    <h5>Mid Web developer</h5>
                    <a href="#">Freelancer.com ( <span class="jobs__location">Dallu , Kathmandu</span> )</a>
                  </div>
                  <p class="post__job">
                    <span class="post__views">Views: 1</span> |
                    <span class="post__deadline">Deadline: {{$employer->deadline}}</span>
                  </p>
                </div>
                <div class="applicant-details clearfix">
                  <p class="text">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perferendis magnam quam corrupti, porro animi unde aliquam ex voluptatem at? Molestiae eos dicta tempore culpa optio consequatur aliquam et tenetur exercitationem.
                  </p>
                </div>
              </div>
              <div class="candidate-details">
                  <div class="toggle-content-client">
                    <h5 class="job__title--sub">Basic Job Information</h5>
                    <ul class="description__list">
                      <li>
                        <p class="description__job">
                          Job Category : <span class="description__job--title">IT & Telecommunication</span>
                        </p>
                      </li>
                      <li >
                        <p class="description__job">
                          Job Level : <span class="description__job--title">{{$employer->level}}</span>
                        </p>
                      </li>
                      <li >
                        <p class="description__job">
                          No. of Vacancy/s : <span class="description__job--title">[{{$employer->no_of_vacancy}}]</span>
                        </p>
                      </li>
                      <li >
                        <p class="description__job">
                          Employment Type : <span class="description__job--title">{{$employer->type}}</span>
                        </p>
                      </li>
                      <li >
                        <p class="description__job">
                          Offered Salary : <span class="description__job--title">NRs.{{$employer->salary}}</span>
                        </p>
                      </li>
                      <li >
                        <p class="description__job">
                          Apply Before (Deadline) : <span class="description__job--title">Aug. 31, 2019 23:55 (2 weeks, 5 days from now)</span>
                        </p>
                      </li>
                    </ul>
                    <h5 class="job__title--sub">Job Description</h5>
                    <p>
                        {{$employer->description}}
                    </p>
                    <div class="skill">
                      <h5>Skill</h5>
                      <div class="skill-list">
                        <ul>
                          <li>
                            <p>{{$employer->skill}}</p>
                            <div class="progress-bar">
                              <div class="progress-bar-inner">
                                <span class="progress7 progress"></span>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                      <div class="addtional">
                        <h5>ADDITIONAL SKILLS & EXPERIENCE</h5>
                        <ul>

                          <li>
                            <p>{{$employer->experience}}</p>
                          </li>
                          
                        </ul>
                      </div>
                    </div>
                    <div class="apply-share">
                      <ul>
                        <li class="share">
                          <strong>
                            Share:
                          </strong>
                        </li>
                        <li class="facebook-color social"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="twitt-color social"><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li class="pinterest-color social"><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="toogle-details">
                      <a class="join--btn candidat-toggle candidat-toggle--name" ><i class="fas fa-chevron-down"></i><span>Show More</span></a>
                      <a class="join--btn " href="">Apply  Now</a>
                  </div>
                </div>
                @endforeach
            </div>



    @include('front.include.script')

           