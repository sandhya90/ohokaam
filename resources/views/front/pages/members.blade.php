<!DOCTYPE html>
<html class="no-js" lang="">
  <head>
  @include('front.include.meta')

      <title>HR|Heavenmaker</title>
      <!-- Owl Carousel start -->
      <link rel="stylesheet" href="{{asset('css\vendors\owl.carousel.min.css')}}">
      <link rel="stylesheet" href="{{asset('css\vendors\owl.theme.default.min.css')}}">
      <!-- Owl Catousel end -->
      @include('front.include.head')

  </head>

  <body class="about">
    <!-- model start-->
     <!-- Extra Large modal -->
     @include('front.include.model')

    <!-- model end -->
    <!--nav button start -->
    @include('front.include.mobile-nav')

    <!-- nav button end -->
    <!-- header  start-->
    @include('front.include.header')

    <!-- header end -->
    <!-- banner start-->
    <section class="banner-page">
      <div class="container-fluid">
        <div class="row">
         <div class="col-1-of-1">
           <h3>Team-Member</h3>
           <div class="breadcrumb">
             <ul>
               <li>
                 <a href="./">Home</a>
               </li>
               <li>
                 <a href="{{route('about')}}"><span>&#47</span>About-Us</a>
               </li>
               <li>
                 <a href="" class="active"><span>&#47</span>Team-Member</a>
               </li>
             </ul>
           </div>
         </div>
        </div>
      </div>
    </section>
    <!-- banner end -->
    <!-- about-team start -->
    <section class="about-team">
      <div class="container-fluid">
        <h3 class="main--header">Meet Our Team Members</h3>
        <p class="about-team--text">
          Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque <br/> laudantium,  totam rem aperiam, eaque ipsa quae
        </p>
        <div class="row">     
          @foreach($team_members as $team_member)       
              <div class="col-1-of-3">
                  <div class="about-team__item"></div>
                      <div class="about-team__header">
                          <figure class="about-team__image">
                              <img src="files/{{$team_member->image}}" alt="team member thumb">
                          </figure>
                      <div class="team__social">
                          <ul>
                              <li class="facebook"><a href="https://www.facebook.com/{{$team_member->facebook}}"><i class="fab fa-facebook-f"></i></a></li>
                              <li class="twitter"><a href="https://twitter.com/{{$team_member->twitter}}"><i class="fab fa-twitter"></i></a></li>
                              <li class="google-plus"><a href="https://plus.google.com/{{$team_member->google}}"><i class="fab fa-google-plus-g"></i></a></li>
                          </ul>
                      </div>
                  </div>
                  <figcaption class="team__content">
                      <h5 class="team__content--header">
                          {{$team_member->name}}
                      </h5>
                      <p class="team__content--text">{{$team_member->post}}</p>
                  </figcaption>
                </div>
            @endforeach

        </div>
        {{$team_members->links()}}

      </div>

        <!-- <a href="" class="btn--category">Load More . . .</a> -->
    </section>
    <!-- about-team end -->
    <!-- footer start -->
    @include('front.include.footer')

   <!-- footer end -->
    <!-- script start -->
    @include('front.include.script')

    <!-- script end -->
  </body>
</html>
