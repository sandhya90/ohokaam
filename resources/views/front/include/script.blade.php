
    <script
      src="https://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
      crossorigin="anonymous"
    ></script>
  
    <!-- To filter employer -->
    <script>
      $(document).ready(function(){
        $('#findBtn').click(function(){
          var cat = $('#catID').val();
          var type = $('#typeID').val();
          var level = $('#levelID').val();
          // alert(cat);
          $.ajax({
            type:'get',
            dataType:'html',
            url:'{{url('/employersCat')}}',
            data: 'cat_id=' + cat + '&type=' + type + '&level=' + level,
            success: function(response){
              console.log(response);
              $('#employerData').html(response);
            }           
          });

        });
      });
    </script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script>
      window.jQuery ||
        document.write(
          '<script src="{{asset('js/vendor/jquery-3.3.1.min.js')}}"><\/script>'
        );
    </script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>

    <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
    <script>
      window.ga = function() {
        ga.q.push(arguments);
      };
      ga.q = [];
      ga.l = +new Date();
      ga('create', 'UA-XXXXX-Y', 'auto');
      ga('send', 'pageview');
    </script>
    <script
      src="https://www.google-analytics.com/analytics.js"
      async
      defer
    ></script>