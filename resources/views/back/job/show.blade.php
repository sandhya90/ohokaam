<!DOCTYPE html>
<html class="no-js" lang="">
  <head>
    @include('front.include.meta')

      <title>HR|Heavenmaker</title>
      
      @include('front.include.head')

      <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/range.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/vendors/ionskin.css')}}">
  </head>

  <body class="category professionals">
    <!-- model start-->
     <!-- Extra Large modal -->
     @include('front.include.model')

    <!-- model end -->
    <!--nav button start -->
    @include('front.include.mobile-nav')

    <!-- nav button end -->
    <!-- header  start-->
    @include('front.include.header')


@foreach($jobs as $job)


<div class="candidate-description clearfix">
              <div class="candidate-description-image">
                <picture>
                  <img src="img\category\it.jpg" alt="img">
                </picture>
              </div>
              <div class="candidate-description-content clearfix">
                <div class="header">
                  <div class="header-left">
                    <h5></h5>
                    <a href="./professionals.php">{{$job->no_of_vacancy}}</a>
                  </div>
                </div>
                <div class="applicant-details clearfix">
                  <p class="text">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perferendis magnam quam corrupti, porro animi unde aliquam ex voluptatem at? Molestiae eos dicta tempore culpa optio consequatur aliquam et tenetur exercitationem.
                  </p>
                </div>
              </div>
              <div class="candidate-details">
                  <div class="toggle-content-client">
                    <h5>Web developer</h5>
                    <p>My passion for programming is in my ability to make tools that make people's lives easier.
                      I love creating value for people, and thrive when I can see the benefit derived from my work
                      as quickly as possible. I believe good work encourages specific behavior, but doesn't necessarily
                      enforce it. My programming style is to make the smallest change necessary to achieve my goal,
                      keep only the most successful work flows and refactor/delete code as part of each change.
                      I always design the interface first and I model client facing solutions as closely as possible
                      to the experience of doing it without a computer, which is typically characterized by loose
                      couplings and graph based models over trees. I keep the clever stuff behind the curtain.
                    </p>
                    <p>
                      Cras vehicula urna non justo adipiscing convallis quis et augue. Proin vestibulum,
                      nisl ut lobortis tempus, est nulla lacinia felis, ut gravida enim nibh vel turpis.
                      Vivamus a purus id ipsum tincidunt faucibus non at elit. Duis imperdiet ullamcorper purus,
                      id tempus dui fringilla porta. Sed lacinia risus eu nulla scelerisque tincidunt. Nam ut velit
                      quis felis pretium pulvinar ac in sem. Nullam nec porttitor velit, sed posuere massa. In gravida
                      mattis dolor sit amet lacinia.
                    </p>
                    <div class="skill">
                      <h5>{{$job->skill}}</h5>
                      <div class="skill-list">
                        <ul>
                          <li>
                            <p>Logo design</p>
                            <div class="progress-bar">
                              <div class="progress-bar-inner">
                                <span class="progress7 progress"></span>
                              </div>
                            </div>
                          </li>
                          <li>
                            <p>Web Design</p>
                            <div class="progress-bar">
                              <div class="progress-bar-inner">
                                <span class="progress6 progress"></span>
                              </div>
                            </div>
                          </li>
                          <li>
                            <p>UX/UI</p>
                            <div class="progress-bar">
                              <div class="progress-bar-inner">
                                <span class="progress4 progress"></span>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                      <div class="addtional">
                        <h5>ADDITIONAL SKILLS & EXPERIENCE</h5>
                        <ul>
                          <li>
                            <p>Work Permit</p>
                          </li>
                          <li>
                            <p>5 Years Ecperience</p>
                          </li>
                          <li>
                            <p>MBA</p>
                          </li>
                          <li>
                            <p>Magento</p>
                          </li>
                          <li>
                            <p>Soft Skill</p>
                          </li>
                          
                        </ul>
                      </div>
                    </div>
                    <div class="apply-share">
                      <ul>
                        <li class="share">
                          <strong>
                            Share:
                          </strong>
                        </li>
                        <li class="facebook-color social"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="twitt-color social"><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li class="pinterest-color social"><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="toogle-details">
                      <a class="join--btn candidat-toggle candidat-toggle--name" ><i class="fas fa-chevron-down"></i><span>Show More</span></a>
                    </div>
                </div>
            </div>
@endforeach




@include('front.include.footer')

<!-- footer end -->
 <!-- script start -->
 @include('front.include.script')

 <script src="js/vendors/range.min.js"></script>
 <script>
    $("#range_28").ionRangeSlider({
         type: "double",
         min: 1 ,
         max: 60 ,
         from: 1,
         to: 60,
         from_min: 1,
         from_max: 60,
         to_min: 1,
         to_max: 60
     });
     $("#range_29").ionRangeSlider({
         type: "double",
         min: 1 ,
         max: 100000 ,
         from: 1,
         to: 100000,
         from_min: 1,
         from_max: 100000,
         to_min: 1,
         to_max: 100000
     });
   </script>
 <!-- script end -->
</body>
</html>