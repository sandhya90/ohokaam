@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['job.update', $job->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    

    <div class = "form-group">
        <label> Sub Category Name</label>
        <input type="string"  name='subcategory_id' value = "{{ old('subcategory_id') ?? $job->subcategory_id }}" class="form-control @error('subcategory_id') is-invalid @enderror" required><br>
        @error('subcategory_id')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    
    <div class = "form-group">
        <label>No of Vacancy</label>
        <input type="integer"  name='no_of_vacancy' value = "{{ old('no_of_vacancy') ?? $job->no_of_vacancy }}" class="form-control @error('no_of_vacancy') is-invalid @enderror" required><br>
        @error('no_of_vacancy')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    
    <div class = "form-group">
        <label> Description</label>
        <input type="text"  name='description' value = "{{ old('description') ?? $job->description }}" class="form-control @error('description') is-invalid @enderror" required><br>
        @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    
    <div class = "form-group">
        <label>Salary</label>
        <input type="decimal"  name='salary' value = "{{ old('salary') ?? $job->salary }}" class="form-control @error('salary') is-invalid @enderror" required><br>
        @error('salary')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    
    <div class = "form-group">
        <label>Job Type</label>
        <input type="string"  name='type' value = "{{ old('type') ?? $job->type }}" class="form-control @error('type') is-invalid @enderror" required><br>
        @error('type')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    
    <div class = "form-group">
        <label> Job Level</label>
        <input type="string"  name='level' value = "{{ old('level') ?? $job->level }}" class="form-control @error('level') is-invalid @enderror" required><br>
        @error('level')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    
    <div class = "form-group">
        <label> Education</label>
        <input type="string"  name='education' value = "{{ old('education') ?? $job->education }}" class="form-control @error('education') is-invalid @enderror" required><br>
        @error('education')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    
    <div class = "form-group">
        <label> Skill</label>
        <input type="string"  name='skill' value = "{{ old('skill') ?? $job->skill }}" class="form-control @error('skill') is-invalid @enderror" required><br>
        @error('skill')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    

    
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection