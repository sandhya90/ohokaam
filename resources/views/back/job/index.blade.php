@extends('back.include.layout')
@section('content')

<!-- for horizontal scrollmenu -->
<style>
    div.scrollmenu {
    /* background-color: #333; */
    overflow: auto;
    white-space: nowrap;
    }

    div.scrollmenu a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px;
    text-decoration: none;
    }

    div.scrollmenu a:hover {
    background-color: #777;
    }
</style>

<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'job.store', 'enctype' => 'multipart/form-data'])}}


<div class="col-md-6">
    <div class="form-group">
        <label for = "maincategory_id">maincategory</label>
        <select name = "maincategory_id" id = "mySelect" type = "string" class="form-control @error('maincategory_id') is-invalid @enderror" required>
            <option value = "">--Select maincategory--</option>
            @foreach($maincategories as $maincategory)
                <option value = "{{$maincategory->id}}">{{$maincategory->name}}</option>
            @endforeach
            @error('maincategory_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </select>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label>SubCategory</label>
        <select name = "subcategory_id" id="SubCategoryList" value = "{{old('subcategory_id')}}" type = "string" class="form-control @error('subcategory_id') is-invalid @enderror" required>
            @error('subcategory_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </select>
    </div>
</div>


<div class = "form-group{{ $errors->has('no_of_vacancy') ? ' has-error' : '' }}">
    <label>No.of Vacancy</label>   
    <input type="integer"  name='no_of_vacancy' value = "{{old('no_of_vacancy')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('no_of_vacancy') }}</small>
</div>

<div class = "form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label>Description</label>   
    <textarea name='description' class="form-control" required>{{old('description')}}</textarea>
    <small class="text-danger">{{ $errors->first('description') }}</small>
  </div>

<div class = "form-group{{ $errors->has('salary') ? ' has-error' : '' }}">
    <label>Salary</label>   
    <input type="decimal"  name='salary' value = "{{old('salary')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('salary') }}</small>
</div>

<div class = "form-group{{ $errors->has('type') ? ' has-error' : '' }}">
    <label>Job Type</label>   
    <input type="string"  name='type' value = "{{old('type')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('type') }}</small>
</div>

<div class = "form-group{{ $errors->has('level') ? ' has-error' : '' }}">
    <label>Job Level</label>   
    <input type="string"  name='level' value = "{{old('level')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('level') }}</small>
</div>

<div class = "form-group{{ $errors->has('education') ? ' has-error' : '' }}">
    <label>Education</label>   
    <input type="string"  name='education' value = "{{old('education')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('education') }}</small>
</div>

<div class = "form-group{{ $errors->has('skill') ? ' has-error' : '' }}">
    <label>Skill</label>   
    <input type="string"  name='skill' value = "{{old('skill')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('skill') }}</small>
</div>

<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Sub Category</h3>

<div class="scrollmenu">
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col">MainCategory Name</th>
            <th scope="col">SubCategory Name</th>
            <th scope="col">Vacancy No.</th>
            <th scope="col">Description</th>
            <th scope="col">Salary</th>
            <th scope="col">Type</th>
            <th scope="col">Level</th>
            <th scope="col">Education</th>
            <th scope="col">Skill</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($jobs as $job)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$job->mainCategory->name}}</td>
            <td>{{$job->subCategory->name}}</td>
            <td>{{$job->no_of_vacancy}}</td>
            <td>{{$job->description}}</td>
            <td>{{$job->salary}}</td>
            <td>{{$job->type}}</td>
            <td>{{$job->level}}</td>
            <td>{{$job->education}}</td>
            <td>{{$job->skill}}</td>

            <td>
              <a href = "{{route('job.edit', $job->id)}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('job.destroy', $job->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach

          
          
        </tbody>
    </table>
    </div>
    

</div>

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous" ></script>
    <script type="text/javascript">

        $('#mySelect').change(function()
            {
                $('#SubCategoryList').empty();
                var id = "";
                $( "#mySelect option:selected " ).each(function() {
                id += $( this ).val() + " ";
                getSubCategoryByajax(id);
                });
            });
        function getSubCategoryByajax(id){
            $.ajax({url: '/admin/getSubCategory/'+id, success: function(data){

                $('#SubCategoryList').append("<option selected disabled > --Select A SubCategory -- </option>");
                data.forEach(function (option) {

                    $('#SubCategoryList').append("<option value=" + option['id'] + ">" + option['name'] + "</option>");
                });
            }

            });
        }
</script>

</div>




@endsection