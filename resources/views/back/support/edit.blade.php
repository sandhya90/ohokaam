@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['support.update', $support->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    
    <div class = "form-group">
        <label> Question</label>
        <input type="text"  name='question' value = "{{ old('question') ?? $support->question }}" class="form-control @error('question') is-invalid @enderror" required><br>
        @error('question')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Answer</label>
        <input type="text"  name='answer' value = "{{ old('answer') ?? $support->answer }}" class="form-control @error('answer') is-invalid @enderror" required><br>
        @error('answer')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection