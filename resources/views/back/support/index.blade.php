@extends('back.include.layout')
@section('content')


<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'support.store', 'enctype' => 'multipart/form-data'])}}


<div class = "form-group{{ $errors->has('question') ? ' has-error' : '' }}">
    <label>Question</label>   
    <textarea name='question' class="form-control" required>{{old('question')}}</textarea>
    <small class="text-danger">{{ $errors->first('question') }}</small>
  </div>

  <div class = "form-group{{ $errors->has('answer') ? ' has-error' : '' }}">
    <label>Answer</label>   
    <textarea name='answer' class="form-control" required>{{old('answer')}}</textarea>
    <small class="text-danger">{{ $errors->first('answer') }}</small>
  </div>

<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Support</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col">Question</th>
            <th scope="col">Answer</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($supports as $support)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$support->question}}</td>
            <td>{{$support->answer}}</td>


            <td>
              <a href = "{{route('support.edit', $support->id )}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('support.destroy', $support->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>

</div>
</div>


@endsection