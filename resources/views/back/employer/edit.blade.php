@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['employer.update', $employer->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    

    <div class = "form-group">
        <label> Company Name</label>
        <input type="string"  name='maincatemployer_id' value = "{{ old('maincatemployer_id') ?? $employer->maincatemployer_id }}" class="form-control @error('maincatemployer_id') is-invalid @enderror" required><br>
        @error('maincatemployer_id')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Post Name</label>
        <input type="checkbox"  name='post_id' value = "{{ old('post_id') ?? $employer->post_id }}" class="form-control @error('post_id') is-invalid @enderror" required><br>
        @error('post_id')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    
    <div class = "form-group">
        <label>No of Jobs</label>
        <input type="integer"  name='no_of_jobs' value = "{{ old('no_of_jobs') ?? $employer->no_of_jobs }}" class="form-control @error('no_of_jobs') is-invalid @enderror" required><br>
        @error('no_of_jobs')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    
    <div class = "form-group">
        <label>Job Type</label>
        <input type="string"  name='type' value = "{{ old('type') ?? $employer->type }}" class="form-control @error('type') is-invalid @enderror" required><br>
        @error('type')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

 

    <div class = "form-group">
        <label>Job Deadline</label>
        <input type="date"  name='deadline' value = "{{ old('deadline') ?? $employer->deadline }}" class="form-control @error('deadline') is-invalid @enderror" required><br>
        @error('deadline')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

          
    <div class = "form-group">
        <label> employer Level</label>
        <input type="string"  name='level' value = "{{ old('level') ?? $employer->level }}" class="form-control @error('level') is-invalid @enderror" required><br>
        @error('level')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    
    <div class = "form-group">
        <label>Salary</label>
        <input type="decimal"  name='salary' value = "{{ old('salary') ?? $employer->salary }}" class="form-control @error('salary') is-invalid @enderror" required><br>
        @error('salary')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>No of Vacancy</label>
        <input type="integer"  name='no_of_vacancy' value = "{{ old('no_of_vacancy') ?? $employer->no_of_vacancy }}" class="form-control @error('no_of_vacancy') is-invalid @enderror" required><br>
        @error('no_of_vacancy')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    
    <div class = "form-group">
        <label> Skill</label>
        <input type="string"  name='skill' value = "{{ old('skill') ?? $employer->skill }}" class="form-control @error('skill') is-invalid @enderror" required><br>
        @error('skill')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

        
    <div class = "form-group">
        <label> Experience</label>
        <input type="string"  name='experience' value = "{{ old('experience') ?? $employer->experience }}" class="form-control @error('experience') is-invalid @enderror" required><br>
        @error('experience')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    
    <div class = "form-group">
        <label> Description</label>
        <input type="text"  name='description' value = "{{ old('description') ?? $employer->description }}" class="form-control @error('description') is-invalid @enderror" required><br>
        @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>


        
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection