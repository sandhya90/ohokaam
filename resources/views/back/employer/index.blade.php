@extends('back.include.layout')
@section('content')

<!-- for horizontal scrollmenu -->
<style>
    div.scrollmenu {
    /* background-color: #333; */
    overflow: auto;
    white-space: nowrap;
    }

    div.scrollmenu a {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px;
    text-decoration: none;
    }

    div.scrollmenu a:hover {
    background-color: #777;
    }
</style>


<div class = "container mt-5">

    {{Form::open(['method'=>'post', 'route'=>'employer.store', 'enctype' => 'multipart/form-data'])}}

    <div class="col-md-6">
    <div class="form-group">
        <label for = "maincatemployer_id">category name</label>
        <select name = "maincatemployer_id" type = "string" class="form-control @error('maincatemployer_id') is-invalid @enderror" required>
            <option value = "">--Select maincatemployer--</option>
            @foreach($maincatemployers as $maincatemployer)
                <option value = "{{$maincatemployer->id}}">{{$maincatemployer->name}}</option>
            @endforeach
            @error('maincatemployer_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </select>
    </div>
</div>

    <div class = "form-group{{ $errors->has('post_id') ? ' has-error' : '' }}">
        <label>Select Post Name</label>
        @foreach($posts as $post)
        <div class="checkbox">
            <label><input type="checkbox" name="post_id[]" value="{{$post->id}}">{{$post->name}}</label>
        </div>
        @endforeach
        <small class="text-danger">{{ $errors->first('post_id') }}</small>
           
    </div>





    <div class = "form-group{{ $errors->has('no_of_jobs') ? ' has-error' : '' }}">
        <label>No.of Jobs</label>   
        <input type="integer"  name='no_of_jobs' value = "{{old('no_of_jobs')}}" class="form-control" required><br>
        <small class="text-danger">{{ $errors->first('no_of_jobs') }}</small>
    </div>


    <div class = "form-group{{ $errors->has('type') ? ' has-error' : '' }}">
        <label>Job Type</label>   
        <input type="string"  name='type' value = "{{old('type')}}" class="form-control" required><br>
        <small class="text-danger">{{ $errors->first('type') }}</small>
    </div>

    <div class = "form-group{{ $errors->has('deadline') ? ' has-error' : '' }}">
        <label>Job Deadline</label>   
        <input type="date"  name='deadline' value = "{{old('deadline')}}" class="form-control" required><br>
        <small class="text-danger">{{ $errors->first('deadline') }}</small>
    </div>


    <div class = "form-group{{ $errors->has('level') ? ' has-error' : '' }}">
        <label>Job Level</label>   
        <input type="string"  name='level' value = "{{old('level')}}" class="form-control" required><br>
        <small class="text-danger">{{ $errors->first('level') }}</small>
    </div>


    <div class = "form-group{{ $errors->has('salary') ? ' has-error' : '' }}">
        <label>Salary</label>   
        <input type="decimal"  name='salary' value = "{{old('salary')}}" class="form-control" required><br>
        <small class="text-danger">{{ $errors->first('salary') }}</small>
    </div>

    <div class = "form-group{{ $errors->has('no_of_vacancy') ? ' has-error' : '' }}">
        <label>No.of Vacancy</label>   
        <input type="integer"  name='no_of_vacancy' value = "{{old('no_of_vacancy')}}" class="form-control" required><br>
        <small class="text-danger">{{ $errors->first('no_of_vacancy') }}</small>
    </div>

    <div class = "form-group{{ $errors->has('skill') ? ' has-error' : '' }}">
        <label>Job Skill</label>   
        <input type="string"  name='skill' value = "{{old('skill')}}" class="form-control" required><br>
        <small class="text-danger">{{ $errors->first('skill') }}</small>
    </div>

    <div class = "form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
        <label>Job Experience</label>   
        <input type="string"  name='experience' value = "{{old('experience')}}" class="form-control" required><br>
        <small class="text-danger">{{ $errors->first('experience') }}</small>
    </div>

    <div class = "form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        <label>Job Description</label>   
        <textarea name='description' class="form-control" required>{{old('description')}}</textarea>
        <small class="text-danger">{{ $errors->first('description') }}</small>
    </div>


    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}


        
</div>



@endsection