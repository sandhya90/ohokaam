@extends('back.include.layout')
@section('content')

<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'post.store', 'enctype' => 'multipart/form-data'])}}


  <!-- <div class = "form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label>Post Name</label>   
    <div class="checkbox">
      <label><input type="checkbox" name="name" value="">Option 1</label>
    </div>
    <small class="text-danger">{{ $errors->first('name') }}</small>

  </div> -->

  <div class = "form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label>Post Name</label>   
    <input type="string"  name='name' value = "{{old('name')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('name') }}</small>
</div>

<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Post Name</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col">Name</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($posts as $post)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$post->name}}</td>



            <td>
              <a href = "{{route('post.edit', $post->id )}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('post.destroy', $post->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>



</div>

                    
@endsection