@extends('back.include.layout')
@section('content')


<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'maincatemployer.store', 'enctype' => 'multipart/form-data'])}}
<div class = "form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label>MainCategory Employer Name</label>   
    <input type="string"  name='name' value = "{{old('name')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('name') }}</small>
</div>

<div class = "form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label>Image</label>   
    <input type="file"  name='image' value = "{{old('image')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('image') }}</small>
</div>

<div class = "form-group{{ $errors->has('location') ? ' has-error' : '' }}">
    <label>Location</label>   
    <input type="string"  name='location' value = "{{old('location')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('location') }}</small>
</div>

<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Main Category</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col">Name</th>
            <th scope="col">Image</th>
            <th scope="col">Location</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($maincatemployers as $maincatemployer)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$maincatemployer->name}}</td>
            <td>{{$maincatemployer->image}}</td>
            <td>{{$maincatemployer->location}}</td>


            <td>
              <a href = "{{route('maincatemployer.edit', $maincatemployer->id )}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('maincatemployer.destroy', $maincatemployer->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>

</div>
</div>


@endsection