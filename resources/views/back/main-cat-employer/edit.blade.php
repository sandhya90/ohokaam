@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['maincatemployer.update', $maincatemployer->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    
    <div class = "form-group">
        <label> MainCategory Employer Name</label>
        <input type="string"  name='name' value = "{{ old('name') ?? $maincatemployer->name }}" class="form-control @error('name') is-invalid @enderror" required><br>
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Image</label>
        <input type="file"  name='image' value = "{{ old('image') ?? $maincatemployer->image }}" class="form-control @error('image') is-invalid @enderror" required><br>
        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Location</label>
        <input type="string"  name='location' value = "{{ old('location') ?? $maincatemployer->location }}" class="form-control @error('location') is-invalid @enderror" required><br>
        @error('location')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection