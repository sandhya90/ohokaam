@extends('back.include.layout')
@section('content')


<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'blog.store', 'enctype' => 'multipart/form-data'])}}
<div class = "form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label>Blog Title</label>   
    <input type="string"  name='title' value = "{{old('title')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('title') }}</small>
</div>

<div class = "form-group{{ $errors->has('date') ? ' has-error' : '' }}">
    <label>Date of Post</label>   
    <input type="date"  name='date' value = "{{old('date')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('date') }}</small>
</div>

<div class = "form-group{{ $errors->has('blogger_name') ? ' has-error' : '' }}">
    <label>Blogger Name</label>   
    <input type="string"  name='blogger_name' value = "{{old('blogger_name')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('blogger_name') }}</small>
</div>


<div class = "form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label>Image of Blog</label>   
    <input type="file"  name='image' value = "{{old('image')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('image') }}</small>
</div>

<div class = "form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label>Blog Description</label>   
    <input type="text"  name='description' value = "{{old('description')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('description') }}</small>
</div>


<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Blog</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col">Title</th>
            <th scope="col">Date</th>
            <th scope="col">Blogger Name</th>
            <th scope="col">Image</th>
            <th scope="col">Description</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($blogs as $blog)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$blog->title}}</td>
            <td>{{$blog->date}}</td>
            <td>{{$blog->blogger_name}}</td>
            <td>{{$blog->image}}</td>
            <td>{{$blog->description}}</td>


            <td>
              <a href = "{{route('blog.edit', $blog->id )}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('blog.destroy', $blog->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>

</div>
</div>


@endsection