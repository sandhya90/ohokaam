@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['blog.update', $blog->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    
    <div class = "form-group">
        <label>Blog Title</label>
        <input type="string"  name='title' value = "{{ old('title') ?? $blog->title }}" class="form-control @error('title') is-invalid @enderror" required><br>
        @error('title')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Date of Post</label>
        <input type="date"  name='date' value = "{{ old('date') ?? $blog->date }}" class="form-control @error('date') is-invalid @enderror" required><br>
        @error('date')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Blogger Name</label>
        <input type="string"  name='blogger_name' value = "{{ old('blogger_name') ?? $blog->blogger_name }}" class="form-control @error('blogger_name') is-invalid @enderror" required><br>
        @error('blogger_name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>


    <div class = "form-group">
        <label>Blog Image</label>
        <input type="file"  name='image' value = "{{ old('image') ?? $blog->image }}" class="form-control @error('image') is-invalid @enderror" required><br>
        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Blog Description</label>
        <input type="text"  name='description' value = "{{ old('description') ?? $blog->description }}" class="form-control @error('description') is-invalid @enderror" required><br>
        @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>


    
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection