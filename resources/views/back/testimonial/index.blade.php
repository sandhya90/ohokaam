@extends('back.include.layout')
@section('content')


<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'testimonial.store', 'enctype' => 'multipart/form-data'])}}
<div class = "form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label>Name</label>   
    <input type="string"  name='name' value = "{{old('name')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('name') }}</small>
</div>

<div class = "form-group{{ $errors->has('post') ? ' has-error' : '' }}">
    <label>Post</label>   
    <input type="string"  name='post' value = "{{old('post')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('post') }}</small>
</div>

<div class = "form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label>Image</label>   
    <input type="file"  name='image' value = "{{old('image')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('image') }}</small>
</div>

<div class = "form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label>Description</label>   
    <input type="text"  name='description' value = "{{old('description')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('description') }}</small>
</div>


<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Testimonial</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col">Name</th>
            <th scope="col">Post</th>
            <th scope="col">Image</th>
            <th scope="col">Description</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($testimonials as $testimonial)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$testimonial->name}}</td>
            <td>{{$testimonial->post}}</td>
            <td>{{$testimonial->image}}</td>
            <td>{{$testimonial->description}}</td>


            <td>
              <a href = "{{route('testimonial.edit', $testimonial->id )}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('testimonial.destroy', $testimonial->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>

</div>
</div>


@endsection