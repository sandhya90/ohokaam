@extends('back.include.layout')
@section('content')


<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'subcategory.store', 'enctype' => 'multipart/form-data'])}}


<div class="col-md-6">
    <div class="form-group">
        <label for = "maincategory_id">maincategory</label>
        <select name = "maincategory_id" type = "string" class="form-control @error('maincategory_id') is-invalid @enderror" required>
            <option value = "">--Select maincategory--</option>
            @foreach($maincategories as $maincategory)
                <option value = "{{$maincategory->id}}">{{$maincategory->name}}</option>
            @endforeach
            @error('maincategory_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </select>
    </div>
</div>

<div class = "form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label>Sub Category Name</label>   
    <input type="string"  name='name' value = "{{old('name')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('subcategory') }}</small>
</div>

<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Sub Category</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col">MainCategory Id</th>
            <th scope="col">SubCategory Name</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($subcategories as $subcategory)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$subcategory->mainCategory->name}}</td>
            <td>{{$subcategory->name}}</td>


            <td>
              <a href = "{{route('subcategory.edit', $subcategory->id )}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('subcategory.destroy', $subcategory->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>

</div>
</div>


@endsection