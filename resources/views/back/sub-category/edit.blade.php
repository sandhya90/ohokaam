@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['subcategory.update', $subcategory->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    
    <div class = "form-group">
        <label> Category Id</label>
        <input type="string"  name='maincategory_id' value = "{{ old('maincategory_id') ?? $subcategory->maincategory_id }}" class="form-control @error('maincategory_id') is-invalid @enderror" required><br>
        @error('maincategory_id')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Sub Category Name</label>
        <input type="string"  name='name' value = "{{ old('name') ?? $subcategory->name }}" class="form-control @error('name') is-invalid @enderror" required><br>
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection