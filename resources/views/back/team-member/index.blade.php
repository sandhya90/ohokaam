@extends('back.include.layout')
@section('content')


<div class = "container mt-5">

{{Form::open(['method'=>'post', 'route'=>'teamMember.store', 'enctype' => 'multipart/form-data'])}}
<div class = "form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label>Team Member Name</label>   
    <input type="string"  name='name' value = "{{old('name')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('name') }}</small>
</div>

<div class = "form-group{{ $errors->has('post') ? ' has-error' : '' }}">
    <label>Post</label>   
    <input type="string"  name='post' value = "{{old('post')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('post') }}</small>
</div>

<div class = "form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label>Image</label>   
    <input type="file"  name='image' value = "{{old('image')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('image') }}</small>
</div>

<div class = "form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
    <label>Facebook</label>   
    <input type="string"  name='facebook' value = "{{old('facebook')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('facebook') }}</small>
</div>

<div class = "form-group{{ $errors->has('twitter') ? ' has-error' : '' }}">
    <label>Twitter</label>   
    <input type="string"  name='twitter' value = "{{old('twitter')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('twitter') }}</small>
</div>

<div class = "form-group{{ $errors->has('google') ? ' has-error' : '' }}">
    <label>Google</label>   
    <input type="string"  name='google' value = "{{old('google')}}" class="form-control" required><br>
    <small class="text-danger">{{ $errors->first('google') }}</small>
</div>



<button class="btn btn-primary btn-sm" type="submit">Submit</button>
{{Form::close()}}


<h3 class="text-center">List of Team member</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col">Name</th>
            <th scope="col">Post</th>
            <th scope="col">Image</th>
            <th scope="col">Facebook</th>
            <th scope="col">Twitter</th>
            <th scope="col">Google</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($teamMembers as $teamMember)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$teamMember->name}}</td>
            <td>{{$teamMember->post}}</td>
            <td>{{$teamMember->image}}</td>
            <td>{{$teamMember->facebook}}</td>
            <td>{{$teamMember->twitter}}</td>
            <td>{{$teamMember->google}}</td>



            <td>
              <a href = "{{route('teamMember.edit', $teamMember->id )}}" class = "btn btn-success btn-sm">Edit</a>
              <a href = "{{route('teamMember.destroy', $teamMember->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>

</div>
</div>


@endsection