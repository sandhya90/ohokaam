@extends('back.include.layout')
@section('content')

<div class = "container mt-5">
    {{Form::open(['method'=>'patch', 'route'=>['teamMember.update', $teamMember->id], 'enctype' => 'multipart/form-data', 'files' => true])}}
    
    <div class = "form-group">
        <label>Team Member Name</label>
        <input type="string"  name='name' value = "{{ old('name') ?? $teamMember->name }}" class="form-control @error('name') is-invalid @enderror" required><br>
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Post</label>
        <input type="string"  name='post' value = "{{ old('post') ?? $teamMember->post }}" class="form-control @error('post') is-invalid @enderror" required><br>
        @error('post')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Image</label>
        <input type="file"  name='image' value = "{{ old('image') ?? $teamMember->image }}" class="form-control @error('image') is-invalid @enderror" required><br>
        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Facebook Name</label>
        <input type="string"  name='facebook' value = "{{ old('facebook') ?? $teamMember->facebook }}" class="form-control @error('facebook') is-invalid @enderror" required><br>
        @error('facebook')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label> Twitter Name</label>
        <input type="string"  name='twitter' value = "{{ old('twitter') ?? $teamMember->twitter }}" class="form-control @error('twitter') is-invalid @enderror" required><br>
        @error('twitter')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class = "form-group">
        <label>Google Name</label>
        <input type="string"  name='google' value = "{{ old('google') ?? $teamMember->google }}" class="form-control @error('google') is-invalid @enderror" required><br>
        @error('google')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>


    
    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
    {{Form::close()}}
</div>

@endsection