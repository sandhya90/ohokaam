<aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('maincategory.create')}}" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Main Category</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('subcategory.create')}}" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Sub Category</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('job.create')}}" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Job </span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('support.create')}}" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Support</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('maincatemployer.create')}}" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">MainCategory Employer</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('employer.create')}}" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Employer</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('teamMember.create')}}" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Team Member</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('testimonial.create')}}" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Testimonial</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('blog.create')}}" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Blog</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('message.create')}}" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Message</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('post.create')}}" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Post</span></a></li>


                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>

        <div class="page-wrapper">

