@extends('back.include.layout')
@section('content')


<div class = "container mt-5">

<h3 class="text-center">List of Message</h3>
<table class="table mt-5">
        <thead>
          <tr>
            <th scope="col">S.N.</th>
            <th scope="col"> Name</th>
            <th scope="col"> Email</th>
            <th scope="col"> Website Name</th>
            <th scope="col"> Message</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @php($i = 1)
          @foreach($messages as $message)
          <tr>
            <th scope="row">{{$i++}}</th>
            <td>{{$message->name}}</td>
            <td>{{$message->email}}</td>            
            <td>{{$message->website_name}}</td>
            <td>{{$message->message}}</td>


            <td>
              <a href = "{{route('message.destroy', $message->id)}}" class = "btn btn-danger btn-sm">Delete</a>

            </td>
          </tr>
          @endforeach
          
        </tbody>
    </table>


</div>
</div>
@endsection